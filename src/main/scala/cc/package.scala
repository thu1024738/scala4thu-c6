/**
  * Created by mac036 on 2017/4/17.
  */
package apps


object FractionApp extends App{
  val frac1=cc.Fraction(1,2)
  val frac2=cc.Fraction(1,3)
  val frac3=
    frac1 + frac2
  println(frac3)
}
